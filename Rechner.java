import java.util.Arrays;
import java.util.Scanner;

public class Rechner {

    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);

        System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

        int x = myScanner.nextInt();

        System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

        int y = myScanner.nextInt();

        final char[] OPERATORS = new char[] { '+', '-', '/', '*' };

        System.out.println("Welche Operation soll ausfgeführt werden?");
        System.out.println(Arrays.toString(OPERATORS));

        final char operation = myScanner.next().charAt(0);

        switch (operation) {
            case '+':
                System.out.print("\n\n\nErgebnis der Addition lautet: ");
                System.out.print(x + " + " + y + " = " + (x + y));
                break;
            case '-':
                System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
                System.out.print(x + " - " + y + " = " + (x - y));
                break;
            case '/':
                System.out.print("\n\n\nErgebnis der Division lautet: ");
                System.out.print(x + " / " + y + " = " + (x / y));
                break;
            case '*':
                System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
                System.out.print(x + " * " + y + " = " + (x * y));
                break;
            default:
                myScanner.close();
                throw new IllegalArgumentException("Unknown operation: " + operation);
        }

        myScanner.close();

    }
}